Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


  devise_scope :user do
      get 'users/sign_out' =>'devise/sessions#destroy'
  authenticated :user do
    root 'chat#index', as: :authenticated_root
  end
  

  unauthenticated do
    root 'devise/sessions#new', as: :unauthenticated_root
  end
  
  end
  
  

   get 'users/index' =>'users#index'
   get 'conversations/index' =>'conversations#index'

    resources :users do 
        

        resources :conversations

    end
    
    resources :conversations do 
        
        resources :messages
    end

  resources :chat


end

class MessagesController < ApplicationController

    
  def create
    @conversation = Conversation.find(params[:conversation_id])
    @message = @conversation.messages.create(message_param)
    @message.save
    
    redirect_to conversation_path(@conversation)
  end
  
  private
  def message_param
     #params.require(:post).permit(:title, :body)
     params.require(:message).permit(:content, :sender) 
  end
    

end

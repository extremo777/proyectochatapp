class ConversationsController < ApplicationController
  def index
    @users=User.all
    @conversations = Conversation.all
  end
  
  def show

    @conversation = Conversation.find(params[:id])
  end



  def create
      
    if Conversation.between(params[:id_receptor], params[:user_id]).present?
        @conversation = Conversation.between(params[:id_receptor], params[:user_id]).first
    else
      
    @conversation = Conversation.create(conversation_params)
    if @conversation.save
      flash[:success] = 'Chat created!'
    end
    end
    redirect_to conversation_path(@conversation)

  end

  private

  def conversation_params

    params.permit(:id_receptor,:user_id )

  end
end

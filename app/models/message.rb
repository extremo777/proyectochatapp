class Message < ApplicationRecord

  belongs_to :conversation,foreign_key: :conversation_id, class_name: 'Conversation'
  belongs_to :user, foreign_key: :sender, class_name: 'User'

end

class Conversation < ApplicationRecord

  belongs_to :user, foreign_key: :user_id, class_name: 'User'
  belongs_to :receptor, foreign_key: :id_receptor, class_name: 'User'
  has_many :messages
  
  scope :between, -> (id_receptor, user_id) do
    where("(conversations.id_receptor = ? AND conversations.user_id = ?) OR (conversations.id_receptor = ? AND conversations.user_id = ?)", id_receptor, user_id, user_id, id_receptor)
    end


end
